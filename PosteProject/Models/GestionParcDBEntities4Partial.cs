﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PosteProject.Models
{
    public partial class GestionParcDBEntities5 : DbContext
    {
        public GestionParcDBEntities5(bool proxyCreationEnabled = true)
            : base("name=GestionParcDBEntities5")
        {
            base.Configuration.ProxyCreationEnabled = true;
        }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PosteProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LReparationE
    {
        public int Id { get; set; }
        public string Service { get; set; }
        public Nullable<int> Montant { get; set; }
    }
}

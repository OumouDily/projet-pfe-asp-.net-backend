﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PosteProject.Models
{
    public partial class ReparationExterne
    {
        public virtual ICollection<LReparationE> LReparationE { get; set; }
        [NotMapped]
        public string DeletedReparationId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PosteProject.Models
{
    public partial class Reparation
    {
        [NotMapped]
        public string DeletedReparationId { get; set; }
        public virtual ICollection<LReparation> LReparation { get; set; }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PosteProject.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sortie
    {
        public int Id { get; set; }
        public string NumeroS { get; set; }
        public Nullable<System.DateTime> Date_Sortie { get; set; }
        public Nullable<int> Numero_Entree { get; set; }
        public Nullable<int> Id_Engin { get; set; }
        public string Observation { get; set; }
    
        public virtual Entree Entree { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class UsersController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        [Authorize(Roles = "Admin,Responsable,Agent")]
        // GET: api/Users
        public System.Object GetUser()
        {
            var result = (from a in db.User
                          where a.Role!="Admin"

                          select new
                          {
                              a.Id,
                              a.Matricule,
                              a.Nom,
                              a.Tel,
                              a.Login,
                              a.Mdp,
                              a.Role,
                              a.Picture
                              

                          }).ToList();
            return result;
        }



        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        //[ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {


            db.User.Add(user);
            try
            {
                db.SaveChanges();
            }

            catch (DbUpdateException)
            {
                if (UserExists(user.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }


            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.User.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }
        [Route("api/Users/DeleteUserImage")]
        public IHttpActionResult DeletePhoto(int userId)
        {
            User user = db.User.Find(userId);
            if (user == null || user.Picture == null)
            {
                return NotFound();
            }
            try
            {
                var filePath = HttpContext.Current.Server.MapPath("~" + user.Picture);
                System.IO.File.Delete(filePath);
            }
            catch { }
            user.Picture = null;
            db.SaveChanges();

            return Ok(user);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.User.Count(e => e.Id == id) > 0;
        }

        [Route("api/Users/PostUserImage")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<HttpResponseMessage> PostUserImage(int userId)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;
                
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    String path = "";
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 5; //Size = 5 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 5 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            string ticks = DateTime.Now.Ticks.ToString() + "-";
                            string imageServerPath = "~/Userimage/" + ticks + postedFile.FileName;
                            var filePath = HttpContext.Current.Server.MapPath("~/Userimage/" + ticks + postedFile.FileName);
                           
                            postedFile.SaveAs(filePath);

                            var currentUser = db.User.FirstOrDefault(u => u.Id == userId);
                            currentUser.Picture = (HttpContext.Current.Request.ApplicationPath +
                  imageServerPath.Substring(1)).Replace("//", "/");
                            db.SaveChanges();

                            path = imageServerPath.Substring(1).Replace("//", "/");

                        }
                    }

                    var message1 = string.Format(path);
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class ReparationsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Reparations
        public System.Object GetReparation()
        {
            var result = (from a in db.Reparation
                          join b in db.Engin on a.Id_Engin equals b.IdE
                          select new
                          {
                              a.IdR,
                              a.Numero,
                              //a.Id_Engin,
                              b.Matricule,
                              a.Observation,
                              a.Total,
                              a.Date
                          }).ToList();
            return result;
        }

        // GET: api/Reparations/5
        [ResponseType(typeof(Reparation))]
        public IHttpActionResult GetReparation(long id)
        {
            var reparation = (from a in db.Reparation
                              where a.IdR == id
                              select new
                              {
                                  a.IdR,
                                  a.Numero,
                                  a.Id_Engin,
                                  //a.Matricule,
                                  a.Observation,
                                  a.Total,
                                  a.Date,
                                  DeletedReparationId = ""
                              }).FirstOrDefault();
            var reparationDetails = (from a in db.LReparation
                                     join b in db.Produit on a.IdP equals b.IdP
                                     where a.IdReparation == id
                                     select new
                                     {
                                         a.IdReparation,
                                         a.Id,
                                         a.IdP,
                                         IdProduit = b.IdP,
                                         b.PU,
                                         a.Quantite,
                                         Total = a.Quantite * b.PU
                                     }).ToList();
            return Ok(new { reparation, reparationDetails });
        }



        // POST: api/Reparations
        [ResponseType(typeof(Reparation))]
        public IHttpActionResult PostReparation(Reparation reparation)
        {

            try
            {
                //Reparation table
                if (reparation.IdR == 0)
                    db.Reparation.Add(reparation);
                else
                    db.Entry(reparation).State = EntityState.Modified;
                //LReparation table
                foreach (var produit in reparation.LReparation)
                {
                    if (produit.Id == 0)
                        db.LReparation.Add(produit);
                    else
                        db.Entry(produit).State = EntityState.Modified;
                    //LReparation table
                }
                //Delete for LReparation
                foreach (var id in reparation.DeletedReparationId.Split(',').Where(x => x != ""))
                {
                    LReparation x = db.LReparation.Find(Convert.ToInt64(id));
                    db.LReparation.Remove(x);
                }
                db.SaveChanges();
                return Ok();



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // DELETE: api/Reparations/5
        [ResponseType(typeof(Reparation))]
        public IHttpActionResult DeleteReparation(long id)
        {
            Reparation reparation = db.Reparation.Include(y => y.LReparation).
                SingleOrDefault(x => x.IdR == id);
            foreach (var produit in reparation.LReparation.ToList())
            {
                db.LReparation.Remove(produit);
            }
            db.Reparation.Remove(reparation);
            db.SaveChanges();

            return Ok(reparation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReparationExists(long id)
        {
            return db.Reparation.Count(e => e.IdR == id) > 0;
        }
    }
}
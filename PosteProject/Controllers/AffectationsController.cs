﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class AffectationsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Affectations
        public IQueryable<Affectation> GetAffectation()
        {
            return db.Affectation;
        }

        // GET: api/Affectations/5
        [ResponseType(typeof(Affectation))]
        public IHttpActionResult GetAffectation(int id)
        {
            Affectation affectation = db.Affectation.Find(id);
            if (affectation == null)
            {
                return NotFound();
            }

            return Ok(affectation);
        }

        // PUT: api/Affectations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAffectation(int id, Affectation affectation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != affectation.IdAff)
            {
                return BadRequest();
            }

            db.Entry(affectation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AffectationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Affectations
        [ResponseType(typeof(Affectation))]
        public IHttpActionResult PostAffectation(Affectation affectation)
        {
            

            db.Affectation.Add(affectation);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = affectation.IdAff }, affectation);
        }

        // DELETE: api/Affectations/5
        [ResponseType(typeof(Affectation))]
        public IHttpActionResult DeleteAffectation(int id)
        {
            Affectation affectation = db.Affectation.Find(id);
            if (affectation == null)
            {
                return NotFound();
            }

            db.Affectation.Remove(affectation);
            db.SaveChanges();

            return Ok(affectation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AffectationExists(int id)
        {
            return db.Affectation.Count(e => e.IdAff == id) > 0;
        }
    }
}
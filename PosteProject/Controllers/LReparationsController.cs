﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class LReparationsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/LReparations
        public IQueryable<LReparation> GetLReparation()
        {
            return db.LReparation;
        }

      
        // PUT: api/LReparations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLReparation(int id, LReparation lReparation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != lReparation.Id)
            {
                return BadRequest();
            }

            db.Entry(lReparation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LReparationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LReparations
        [ResponseType(typeof(LReparation))]
        public IHttpActionResult PostLReparation(LReparation lReparation)
        {
           

            db.LReparation.Add(lReparation);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = lReparation.Id }, lReparation);
        }

        // DELETE: api/LReparations/5
        [ResponseType(typeof(LReparation))]
        public IHttpActionResult DeleteLReparation(int id)
        {
            LReparation lReparation = db.LReparation.Find(id);
            if (lReparation == null)
            {
                return NotFound();
            }

            db.LReparation.Remove(lReparation);
            db.SaveChanges();

            return Ok(lReparation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LReparationExists(int id)
        {
            return db.LReparation.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class TypeEnginsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/TypeEngins
        public IQueryable<TypeEngin> GetTypeEngin()
        {
            return db.TypeEngin;
        }

      

        // PUT: api/TypeEngins/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTypeEngin(int id, TypeEngin typeEngin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != typeEngin.Id)
            {
                return BadRequest();
            }

            db.Entry(typeEngin).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeEnginExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TypeEngins
        [ResponseType(typeof(TypeEngin))]
        public IHttpActionResult PostTypeEngin(TypeEngin typeEngin)
        {
           
            db.TypeEngin.Add(typeEngin);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = typeEngin.Id }, typeEngin);
        }

        // DELETE: api/TypeEngins/5
        [ResponseType(typeof(TypeEngin))]
        public IHttpActionResult DeleteTypeEngin(int id)
        {
            TypeEngin typeEngin = db.TypeEngin.Find(id);
            if (typeEngin == null)
            {
                return NotFound();
            }

            db.TypeEngin.Remove(typeEngin);
            db.SaveChanges();

            return Ok(typeEngin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TypeEnginExists(int id)
        {
            return db.TypeEngin.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class SortiesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Sorties
        public System.Object GetSortie()
        {
            var result = (from a in db.Sortie
                          join b in db.Engin on a.Id_Engin equals b.IdE
                          join c in db.Entree on a.Numero_Entree equals c.IdEn
                          select new
                          {
                              a.Id,
                              a.NumeroS,
                              a.Date_Sortie,
                              c.Numero,
                              b.Matricule,
                              a.Observation
                           
                          }).ToList();
            return result;
        }



        // PUT: api/Sorties/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSortie(int id, Sortie sortie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sortie.Id)
            {
                return BadRequest();
            }

            db.Entry(sortie).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SortieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sorties
        [ResponseType(typeof(Sortie))]
        public IHttpActionResult PostSortie(Sortie sortie)
        {
           

            db.Sortie.Add(sortie);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sortie.Id }, sortie);
        }

        // DELETE: api/Sorties/5
        [ResponseType(typeof(Sortie))]
        public IHttpActionResult DeleteSortie(int id)
        {
            Sortie sortie = db.Sortie.Find(id);
            if (sortie == null)
            {
                return NotFound();
            }

            db.Sortie.Remove(sortie);
            db.SaveChanges();

            return Ok(sortie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SortieExists(int id)
        {
            return db.Sortie.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class EntreesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Entrees
        public System.Object GetEntree()
        {
            var result = (from a in db.Entree
                          join b in db.Engin on a.Id_Engin equals b.IdE
                          join c in db.Motif on a.Id_Motif equals c.Id
                          select new
                          {
                              a.IdEn,
                              a.Numero,
                              a.Date_Entree,
                              b.Matricule,
                              c.LibelleMotif,
                              a.Observation,
                              a.Index,
                              a.Niveau
                          }).ToList();
            return result;
        }



        // PUT: api/Entrees/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEntree(int id, Entree entree)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entree.IdEn)
            {
                return BadRequest();
            }

            db.Entry(entree).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntreeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Entrees
        [ResponseType(typeof(Entree))]
        public IHttpActionResult PostEntree(Entree entree)
        {
           

            db.Entree.Add(entree);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = entree.IdEn }, entree);
        }

        // DELETE: api/Entrees/5
        [ResponseType(typeof(Entree))]
        public IHttpActionResult DeleteEntree(int id)
        {
            Entree entree = db.Entree.Find(id);
            if (entree == null)
            {
                return NotFound();
            }

            db.Entree.Remove(entree);
            db.SaveChanges();

            return Ok(entree);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EntreeExists(int id)
        {
            return db.Entree.Count(e => e.IdEn == id) > 0;
        }
    }
}
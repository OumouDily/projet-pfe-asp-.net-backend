﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class MotifsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Motifs
        public IQueryable<Motif> GetMotif()
        {
            return db.Motif;
        }

      
        // PUT: api/Motifs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMotif(int id, Motif motif)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != motif.Id)
            {
                return BadRequest();
            }

            db.Entry(motif).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MotifExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Motifs
        [ResponseType(typeof(Motif))]
        public IHttpActionResult PostMotif(Motif motif)
        {
           

            db.Motif.Add(motif);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = motif.Id }, motif);
        }

        // DELETE: api/Motifs/5
        [ResponseType(typeof(Motif))]
        public IHttpActionResult DeleteMotif(int id)
        {
            Motif motif = db.Motif.Find(id);
            if (motif == null)
            {
                return NotFound();
            }

            db.Motif.Remove(motif);
            db.SaveChanges();

            return Ok(motif);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MotifExists(int id)
        {
            return db.Motif.Count(e => e.Id == id) > 0;
        }
    }
}
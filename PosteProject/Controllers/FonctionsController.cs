﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class FonctionsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Fonctions
        public IQueryable<Fonction> GetFonction()
        {
            return db.Fonction;
        }

      

        // PUT: api/Fonctions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFonction(int id, Fonction fonction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fonction.IdF)
            {
                return BadRequest();
            }

            db.Entry(fonction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FonctionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Fonctions
        [ResponseType(typeof(Fonction))]
        public IHttpActionResult PostFonction(Fonction fonction)
        {
           

            db.Fonction.Add(fonction);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = fonction.IdF }, fonction);
        }

        // DELETE: api/Fonctions/5
        [ResponseType(typeof(Fonction))]
        public IHttpActionResult DeleteFonction(int id)
        {
            Fonction fonction = db.Fonction.Find(id);
            if (fonction == null)
            {
                return NotFound();
            }

            db.Fonction.Remove(fonction);
            db.SaveChanges();

            return Ok(fonction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FonctionExists(int id)
        {
            return db.Fonction.Count(e => e.IdF == id) > 0;
        }
    }
}
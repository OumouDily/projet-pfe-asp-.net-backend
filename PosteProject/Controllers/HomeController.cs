﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using CrystalDecisions.CrystalReports.Engine;


using System.IO;
using PosteProject.Models;


namespace PosteProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        #region LoadData
        public JsonResult LoadData(Engin Param)
        {
            List<Engin> EnginList = new List<Engin>();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString))
            {
                var cmd = new SqlCommand("Engin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Matricule", SqlDbType.VarChar)).Value = Param.Matricule;
                cmd.Parameters.Add(new SqlParameter("@Mat_Origine", SqlDbType.VarChar)).Value = Param.Mat_Origine;
                try
                {
                    con.Open();
                    using (SqlDataReader DbReader = cmd.ExecuteReader())
                        if (DbReader.HasRows)
                        {
                            while (DbReader.Read())
                            {
                                Engin En = new Engin();
                                En.Matricule = DbReader.GetString(DbReader.GetOrdinal("Matricule"));
                                En.Nature = DbReader.GetString(DbReader.GetOrdinal("Nature"));
                                En.Mat_Origine = DbReader.GetString(DbReader.GetOrdinal("Mat_Origine"));
                               
                                EnginList.Add(En);
                            }
                        }
                    return Json(EnginList, JsonRequestBehavior.AllowGet);
                }
                finally
                {
                    con.Close();
                }
            }
        }
        #endregion

        [HttpPost]
        #region EditData
        public string EditData(Engin Param)
        {
            if (Param != null)
            {
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString))
                {
                    var cmd = new SqlCommand("Engin", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Matricule", SqlDbType.VarChar)).Value = Param.Matricule;
                    cmd.Parameters.Add(new SqlParameter("@Mat_Origine", SqlDbType.VarChar)).Value = Param.Mat_Origine;
                    cmd.Parameters.Add(new SqlParameter("@Nature", SqlDbType.VarChar)).Value = Param.Nature;


                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        return "Success";
                    }
                    catch (Exception ex)
                    {
                        return ex.ToString();
                    }
                    finally
                    {
                        if (con.State != ConnectionState.Closed)
                            con.Close();

                    }

                }
            }

            else
            {
                return "Model Error";
            }
        }
        #endregion

        public ActionResult ExportExcel()
        {
            List<Engin> BookList = new List<Engin>();
            DataSetReportt DsReport = new DataSetReportt();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString))
            {
                var cmd = new SqlCommand("Engin,", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Matricule", SqlDbType.VarChar)).Value = "GET";
                con.Open();

              //  (new SqlDataAdapter(cmd)).Fill(DsReport.Tables["EnginList"]);
            }

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "CrystalReport3.rpt"));
          //rd.SetDataSource(DsReport.Tables["EnginList"]);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CrystalReport3.xlsx");
        }
        public ActionResult ExportPdf()
        {
            List<Engin> EnginList = new List<Engin>();
            DataSetReportt DsReport = new DataSetReportt();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString))
            {
                var cmd = new SqlCommand("Engin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Matricule", SqlDbType.VarChar)).Value = "GET";
                con.Open();

              // (new SqlDataAdapter(cmd)).Fill(DsReport.Tables["EnginList"]);
            }

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Report"), "CrystalReport3.rpt"));
           // rd.SetDataSource(DsReport.Tables["EnginList"]);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CrystalReport3.Pdf");
        }


    }

    internal class DataSetReportt
    {
    }
}

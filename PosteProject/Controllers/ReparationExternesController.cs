﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class ReparationExternesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/ReparationExternes
        public System.Object GetReparationExterne()
        {
            var result = (from a in db.ReparationExterne
                          join b in db.Engin on a.Id_Engin equals b.IdE
                          join c in db.Fournisseur on a.Fournisseur equals c.IdFo

                          select new
                          {
                              a.IdRE,
                              c.LibelleFournisseur,
                             
                              b.Matricule,
                              a.Total,
                              a.DateR
                          }).ToList();
            return result;
        }

        // GET: api/ReparationExternes/5
        [ResponseType(typeof(ReparationExterne))]
        public IHttpActionResult GetReparationExterne(int id)
        {
            var reparationE = (from a in db.ReparationExterne
                               where a.IdRE == id
                               select new
                               {
                                   a.IdRE,
                                   a.Fournisseur,
                                   a.Id_Engin,

                                   a.Total,
                                   a.DateR,
                                   DeletedReparationId = ""
                               }).FirstOrDefault();
            var reparationEDetails = (from a in db.LReparationE

                                      where a.Id == id
                                      select new
                                      {

                                          a.Id,
                                          a.Service,
                                          a.Montant
                                      }).ToList();
            return Ok(new { reparationE, reparationEDetails });
        }

       

        // POST: api/ReparationExternes
        [ResponseType(typeof(ReparationExterne))]
        public IHttpActionResult PostReparationExterne(ReparationExterne reparationExterne)
        {
            try
            {
                //Reparation table
                if (reparationExterne.IdRE == 0)
                    db.ReparationExterne.Add(reparationExterne);
                else
                    db.Entry(reparationExterne).State = EntityState.Modified;
                //LReparation table
                foreach (var service in reparationExterne.LReparationE)
                {
                    if (service.Id == 0)
                        db.LReparationE.Add(service);
                    else
                        db.Entry(service).State = EntityState.Modified;
                    //LReparation table
                }
                //Delete for LReparation
                foreach (var id in reparationExterne.DeletedReparationId.Split(',').Where(x => x != ""))
                {
                    LReparationE x = db.LReparationE.Find(Convert.ToInt64(id));
                    db.LReparationE.Remove(x);
                }
                db.SaveChanges();
                return Ok();



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // DELETE: api/ReparationExternes/5
        [ResponseType(typeof(ReparationExterne))]
        public IHttpActionResult DeleteReparationExterne(int id)
        {
            ReparationExterne reparationExterne = db.ReparationExterne.Find(id);
            if (reparationExterne == null)
            {
                return NotFound();
            }

            db.ReparationExterne.Remove(reparationExterne);
            db.SaveChanges();

            return Ok(reparationExterne);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReparationExterneExists(int id)
        {
            return db.ReparationExterne.Count(e => e.IdRE == id) > 0;
        }
    }
}
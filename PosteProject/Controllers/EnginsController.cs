﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class EnginsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5();

        // GET: api/Engins
        public System.Object GetEngin()
        {
            var result = (from a in db.Engin
                              //  join f in db.Entree on a.IndexE equals f.IdEn
                          join b in db.TypeEngin on a.Id_Type equals b.Id
                          join c in db.Model on a.Id_Model equals c.IdM
                          join d in db.Marque on a.Id_Marque equals d.IdMa
                          join e in db.Direction on a.Id_Direction equals e.IdD

                          select new
                          {
                              a.IdE,
                              a.Matricule,
                              a.Date_Acquis,
                              a.Val_Acquis,
                              a.Date_Mise_Service,
                              a.Nature,
                              a.NSerie,
                              a.Date_Visite_Technique,
                              a.Date_Reparation,
                              a.PU,
                              a.Nbre_Place,
                              // f.Index,
                              a.Mat_Origine,
                              b.LibelleTypeEngin,
                              c.LibelleModel,
                              d.LibelleMarque,
                              e.LibelleDirection,
                              a.numeroEntree,
                              a.numeroSortie

                          }).ToList();
            return result;
        }

        // GET: api/Engins/5
        [ResponseType(typeof(Engin))]
        public IHttpActionResult GetEngin(int id)
        {
            var engin = (from a in db.Engin

                             //join f in db.Entree on a.IndexE equals f.IdEn

                         join b in db.TypeEngin on a.Id_Type equals b.Id
                         join c in db.Model on a.Id_Model equals c.IdM
                         join d in db.Marque on a.Id_Marque equals d.IdMa
                         join e in db.Direction on a.Id_Direction equals e.IdD
                         where a.Nature == "Reserve"


                         select new
                         {
                             a.IdE,
                             a.Matricule,
                             a.Date_Acquis,
                             a.Val_Acquis,
                             a.Date_Mise_Service,
                             a.Nature,
                             a.NSerie,
                             a.Date_Visite_Technique,
                             a.Date_Reparation,
                             a.PU,
                             a.Nbre_Place,
                             //    f.Index,
                             a.Mat_Origine,
                             b.LibelleTypeEngin,
                             c.LibelleModel,
                             d.LibelleMarque,
                             e.LibelleDirection,
                             a.numeroEntree,
                             a.numeroSortie

                         }).ToList();

            return Ok(new { engin });
        }

        // PUT: api/Engins/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEngin(int id, Engin engin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != engin.IdE)
            {
                return BadRequest();
            }

            db.Entry(engin).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EnginExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Engins
        [ResponseType(typeof(Engin))]
        public IHttpActionResult PostEngin(Engin engin)
        {
            

            db.Engin.Add(engin);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = engin.IdE }, engin);
        }

        // DELETE: api/Engins/5
        [ResponseType(typeof(Engin))]
        public IHttpActionResult DeleteEngin(int id)
        {
            Engin engin = db.Engin.Find(id);
            if (engin == null)
            {
                return NotFound();
            }

            db.Engin.Remove(engin);
            db.SaveChanges();

            return Ok(engin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EnginExists(int id)
        {
            return db.Engin.Count(e => e.IdE == id) > 0;
        }
    }
}
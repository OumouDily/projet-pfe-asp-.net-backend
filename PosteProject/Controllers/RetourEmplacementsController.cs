﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class RetourEmplacementsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/RetourEmplacements
        public IQueryable<RetourEmplacement> GetRetourEmplacement()
        {
            return db.RetourEmplacement;
        }

       

        // PUT: api/RetourEmplacements/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRetourEmplacement(int id, RetourEmplacement retourEmplacement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != retourEmplacement.Id)
            {
                return BadRequest();
            }

            db.Entry(retourEmplacement).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RetourEmplacementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RetourEmplacements
        [ResponseType(typeof(RetourEmplacement))]
        public IHttpActionResult PostRetourEmplacement(RetourEmplacement retourEmplacement)
        {
           

            db.RetourEmplacement.Add(retourEmplacement);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = retourEmplacement.Id }, retourEmplacement);
        }

        // DELETE: api/RetourEmplacements/5
        [ResponseType(typeof(RetourEmplacement))]
        public IHttpActionResult DeleteRetourEmplacement(int id)
        {
            RetourEmplacement retourEmplacement = db.RetourEmplacement.Find(id);
            if (retourEmplacement == null)
            {
                return NotFound();
            }

            db.RetourEmplacement.Remove(retourEmplacement);
            db.SaveChanges();

            return Ok(retourEmplacement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RetourEmplacementExists(int id)
        {
            return db.RetourEmplacement.Count(e => e.Id == id) > 0;
        }
    }
}
﻿
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    [AllowAnonymous]
    public class AuthController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        private string createToken(string username, string role)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);

            //http://stackoverflow.com/questions/18223868/how-to-encrypt-jwt-security-token
            var tokenHandler = new JwtSecurityTokenHandler();

            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Role, role)
            });

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(
                         issuer: "http://localhost:63003",
                        audience: "http://localhost:63003",
                        subject: claimsIdentity,
                        notBefore: issuedAt,
                        expires: expires,
                        signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }


        //[ResponseType(typeof(User))]
        [HttpPost] 
        //[Route("auth")]
        public HttpResponseMessage GetUser([FromBody] User userLogin)
        {
            var user = db.User.FirstOrDefault(u => u.Login == userLogin.Login && u.Mdp == userLogin.Mdp);

            if (user != null)
            {
                user.Token = createToken(user.Login, user.Role);
                //return Ok(new { user });
                return ControllerContext.Request.CreateResponse(HttpStatusCode.OK, user, "application/json");
            }
            else
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Oops!!!" };
                // throw new HttpResponseException(msg);
                return ControllerContext.Request.CreateResponse(HttpStatusCode.Unauthorized, user, "application/json");
            }
        }

       

    }
}

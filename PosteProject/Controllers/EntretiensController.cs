﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class EntretiensController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Entretiens
        public IQueryable<Entretien> GetEntretien()
        {
            return db.Entretien;
        }

        // GET: api/Entretiens/5
        [ResponseType(typeof(Entretien))]
        public IHttpActionResult GetEntretien(int id)
        {
            Entretien entretien = db.Entretien.Find(id);
            if (entretien == null)
            {
                return NotFound();
            }

            return Ok(entretien);
        }

        // PUT: api/Entretiens/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEntretien(int id, Entretien entretien)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entretien.IdEnt)
            {
                return BadRequest();
            }

            db.Entry(entretien).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EntretienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Entretiens
        [ResponseType(typeof(Entretien))]
        public IHttpActionResult PostEntretien(Entretien entretien)
        {
            

            db.Entretien.Add(entretien);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = entretien.IdEnt }, entretien);
        }

        // DELETE: api/Entretiens/5
        [ResponseType(typeof(Entretien))]
        public IHttpActionResult DeleteEntretien(int id)
        {
            Entretien entretien = db.Entretien.Find(id);
            if (entretien == null)
            {
                return NotFound();
            }

            db.Entretien.Remove(entretien);
            db.SaveChanges();

            return Ok(entretien);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EntretienExists(int id)
        {
            return db.Entretien.Count(e => e.IdEnt == id) > 0;
        }
    }
}
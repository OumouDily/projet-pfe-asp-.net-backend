﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class SfamillesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Sfamilles
        public System.Object GetSfamille()
        {
            var result = (from a in db.Sfamille
                          join b in db.Famille on a.Id_Famille equals b.IdFa
                         
                          select new
                          {
                              a.IdSf,
           
                              a.LibelleSFamille,
                              b.LibelleFamille
                           

                          }).ToList();
            return result;
        }

      

        // PUT: api/Sfamilles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSfamille(int id, Sfamille sfamille)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sfamille.IdSf)
            {
                return BadRequest();
            }

            db.Entry(sfamille).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SfamilleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sfamilles
        [ResponseType(typeof(Sfamille))]
        public IHttpActionResult PostSfamille(Sfamille sfamille)
        {
          

            db.Sfamille.Add(sfamille);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sfamille.IdSf }, sfamille);
        }

        // DELETE: api/Sfamilles/5
        [ResponseType(typeof(Sfamille))]
        public IHttpActionResult DeleteSfamille(int id)
        {
            Sfamille sfamille = db.Sfamille.Find(id);
            if (sfamille == null)
            {
                return NotFound();
            }

            db.Sfamille.Remove(sfamille);
            db.SaveChanges();

            return Ok(sfamille);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SfamilleExists(int id)
        {
            return db.Sfamille.Count(e => e.IdSf == id) > 0;
        }
    }
}
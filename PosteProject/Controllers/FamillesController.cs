﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class FamillesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Familles
        public IQueryable<Famille> GetFamille()
        {
            return db.Famille;
        }

      

        // PUT: api/Familles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFamille(int id, Famille famille)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != famille.IdFa)
            {
                return BadRequest();
            }

            db.Entry(famille).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FamilleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Familles
        [ResponseType(typeof(Famille))]
        public IHttpActionResult PostFamille(Famille famille)
        {

            db.Famille.Add(famille);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = famille.IdFa }, famille);
        }

        // DELETE: api/Familles/5
        [ResponseType(typeof(Famille))]
        public IHttpActionResult DeleteFamille(int id)
        {
            Famille famille = db.Famille.Find(id);
            if (famille == null)
            {
                return NotFound();
            }

            db.Famille.Remove(famille);
            db.SaveChanges();

            return Ok(famille);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FamilleExists(int id)
        {
            return db.Famille.Count(e => e.IdFa == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class ConsommationsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Consommations
        public IQueryable<Consommation> GetConsommation()
        {
            return db.Consommation;
        }
        [Route("api/Consommations/GetConsomMonthlyByNature")]
        [HttpGet]
        public HttpResponseMessage GetConsomMonthlyByNature()
        {
            var conso = db.Consommation.ToList()
                                        .GroupBy(c => new { c.Date_nvt.Value.Month, c.Date_nvt.Value.Year, c.Nature })
                                        .Select(c => new { MonthDate = new DateTime(c.Key.Year, c.Key.Month, 1), Nature = c.Key.Nature, Litres = c.Sum(x => x.NbreL) });
            return this.Request.CreateResponse(HttpStatusCode.OK, conso);
        }
        [Route("api/Consommations/GetConsomMonthlyByDirection")]
        [HttpGet]
        public HttpResponseMessage GetConsomMonthlyByDirection()
        {
            var conso = db.Consommation.ToList()
                                        .GroupBy(c => new { c.Date_nvt.Value.Month, c.Date_nvt.Value.Year,c.Direction.LibelleDirection, c.Nature })
                                       .Select(c => new { MonthDate = new DateTime(c.Key.Year, c.Key.Month, 1),Direction = c.Key.LibelleDirection, Nature = c.Key.Nature, Litres = c.Sum(x => x.NbreL) });
            return this.Request.CreateResponse(HttpStatusCode.OK, conso);
        }

        // GET: api/Consommations/5
        [ResponseType(typeof(Consommation))]
        public IHttpActionResult GetConsommation(int id)
        {
            Consommation consommation = db.Consommation.Find(id);
            if (consommation == null)
            {
                return NotFound();
            }

            return Ok(consommation);
        }

        // PUT: api/Consommations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutConsommation(int id, Consommation consommation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != consommation.Id)
            {
                return BadRequest();
            }

            db.Entry(consommation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsommationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Consommations
        [ResponseType(typeof(Consommation))]
        public IHttpActionResult PostConsommation(Consommation consommation)
        {


            db.Consommation.Add(consommation);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = consommation.Id }, consommation);
        }

        // DELETE: api/Consommations/5
        [ResponseType(typeof(Consommation))]
        public IHttpActionResult DeleteConsommation(int id)
        {
            Consommation consommation = db.Consommation.Find(id);
            if (consommation == null)
            {
                return NotFound();
            }

            db.Consommation.Remove(consommation);
            db.SaveChanges();

            return Ok(consommation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConsommationExists(int id)
        {
            return db.Consommation.Count(e => e.Id == id) > 0;
        }
    }
}
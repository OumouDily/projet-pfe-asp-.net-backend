﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class RemplacementsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Remplacements
        public IQueryable<Remplacement> GetRemplacement()
        {
            return db.Remplacement;
        }

       

        // PUT: api/Remplacements/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRemplacement(int id, Remplacement remplacement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != remplacement.IdRemp)
            {
                return BadRequest();
            }

            db.Entry(remplacement).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RemplacementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Remplacements
        [ResponseType(typeof(Remplacement))]
        public IHttpActionResult PostRemplacement(Remplacement remplacement)
        {
           

            db.Remplacement.Add(remplacement);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = remplacement.IdRemp }, remplacement);
        }

        // DELETE: api/Remplacements/5
        [ResponseType(typeof(Remplacement))]
        public IHttpActionResult DeleteRemplacement(int id)
        {
            Remplacement remplacement = db.Remplacement.Find(id);
            if (remplacement == null)
            {
                return NotFound();
            }

            db.Remplacement.Remove(remplacement);
            db.SaveChanges();

            return Ok(remplacement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RemplacementExists(int id)
        {
            return db.Remplacement.Count(e => e.IdRemp == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class ResidencesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Residences
        public IQueryable<Residence> GetResidence()
        {
            return db.Residence;
        }

     

        // PUT: api/Residences/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutResidence(int id, Residence residence)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != residence.IdR)
            {
                return BadRequest();
            }

            db.Entry(residence).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResidenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Residences
        [ResponseType(typeof(Residence))]
        public IHttpActionResult PostResidence(Residence residence)
        {
           

            db.Residence.Add(residence);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = residence.IdR }, residence);
        }

        // DELETE: api/Residences/5
        [ResponseType(typeof(Residence))]
        public IHttpActionResult DeleteResidence(int id)
        {
            Residence residence = db.Residence.Find(id);
            if (residence == null)
            {
                return NotFound();
            }

            db.Residence.Remove(residence);
            db.SaveChanges();

            return Ok(residence);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ResidenceExists(int id)
        {
            return db.Residence.Count(e => e.IdR == id) > 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class DirectionsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Directions
        public IQueryable<Direction> GetDirection()
        {
            return db.Direction;
        }

        // PUT: api/Directions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDirection(int id, Direction direction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != direction.IdD)
            {
                return BadRequest();
            }

            db.Entry(direction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DirectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Directions
        [ResponseType(typeof(Direction))]
        public IHttpActionResult PostDirection(Direction direction)
        {
          

            db.Direction.Add(direction);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = direction.IdD }, direction);
        }

        // DELETE: api/Directions/5
        [ResponseType(typeof(Direction))]
        public IHttpActionResult DeleteDirection(int id)
        {
            Direction direction = db.Direction.Find(id);
            if (direction == null)
            {
                return NotFound();
            }

            db.Direction.Remove(direction);
            db.SaveChanges();

            return Ok(direction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DirectionExists(int id)
        {
            return db.Direction.Count(e => e.IdD == id) > 0;
        }




       
    }

}
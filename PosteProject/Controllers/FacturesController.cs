﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class FacturesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Factures
        public IQueryable<Facture> GetFacture()
        {
            return db.Facture;
        }

      

        // PUT: api/Factures/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFacture(int id, Facture facture)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != facture.Id)
            {
                return BadRequest();
            }

            db.Entry(facture).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FactureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Factures
        [ResponseType(typeof(Facture))]
        public IHttpActionResult PostFacture(Facture facture)
        {
            

            db.Facture.Add(facture);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = facture.Id }, facture);
        }

        // DELETE: api/Factures/5
        [ResponseType(typeof(Facture))]
        public IHttpActionResult DeleteFacture(int id)
        {
            Facture facture = db.Facture.Find(id);
            if (facture == null)
            {
                return NotFound();
            }

            db.Facture.Remove(facture);
            db.SaveChanges();

            return Ok(facture);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FactureExists(int id)
        {
            return db.Facture.Count(e => e.Id == id) > 0;
        }
    }
}
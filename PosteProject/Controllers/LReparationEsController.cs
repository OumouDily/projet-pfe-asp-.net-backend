﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class LReparationEsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/LReparationEs
        public IQueryable<LReparationE> GetLReparationE()
        {
            return db.LReparationE;
        }

      

        // PUT: api/LReparationEs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLReparationE(int id, LReparationE lReparationE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != lReparationE.Id)
            {
                return BadRequest();
            }

            db.Entry(lReparationE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LReparationEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LReparationEs
        [ResponseType(typeof(LReparationE))]
        public IHttpActionResult PostLReparationE(LReparationE lReparationE)
        {
           

            db.LReparationE.Add(lReparationE);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = lReparationE.Id }, lReparationE);
        }

        // DELETE: api/LReparationEs/5
        [ResponseType(typeof(LReparationE))]
        public IHttpActionResult DeleteLReparationE(int id)
        {
            LReparationE lReparationE = db.LReparationE.Find(id);
            if (lReparationE == null)
            {
                return NotFound();
            }

            db.LReparationE.Remove(lReparationE);
            db.SaveChanges();

            return Ok(lReparationE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LReparationEExists(int id)
        {
            return db.LReparationE.Count(e => e.Id == id) > 0;
        }
    }
}
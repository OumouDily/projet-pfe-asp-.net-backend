﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class CarburantsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Carburants
        public IQueryable<Carburant> GetCarburant()
        {
            return db.Carburant;
        }


        // PUT: api/Carburants/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCarburant(int id, Carburant carburant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != carburant.IdC)
            {
                return BadRequest();
            }

            db.Entry(carburant).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarburantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Carburants
        [ResponseType(typeof(Carburant))]
        public IHttpActionResult PostCarburant(Carburant carburant)
        {

            db.Carburant.Add(carburant);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = carburant.IdC }, carburant);
        }

        // DELETE: api/Carburants/5
        [ResponseType(typeof(Carburant))]
        public IHttpActionResult DeleteCarburant(int id)
        {
            Carburant carburant = db.Carburant.Find(id);
            if (carburant == null)
            {
                return NotFound();
            }

            db.Carburant.Remove(carburant);
            db.SaveChanges();

            return Ok(carburant);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CarburantExists(int id)
        {
            return db.Carburant.Count(e => e.IdC == id) > 0;
        }
    }
}
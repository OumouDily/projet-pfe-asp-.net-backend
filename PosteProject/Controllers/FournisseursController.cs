﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class FournisseursController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Fournisseurs
        public IQueryable<Fournisseur> GetFournisseur()
        {
            return db.Fournisseur;
        }

       

        // PUT: api/Fournisseurs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFournisseur(int id, Fournisseur fournisseur)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fournisseur.IdFo)
            {
                return BadRequest();
            }

            db.Entry(fournisseur).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FournisseurExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Fournisseurs
        [ResponseType(typeof(Fournisseur))]
        public IHttpActionResult PostFournisseur(Fournisseur fournisseur)
        {
           
            db.Fournisseur.Add(fournisseur);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = fournisseur.IdFo }, fournisseur);
        }

        // DELETE: api/Fournisseurs/5
        [ResponseType(typeof(Fournisseur))]
        public IHttpActionResult DeleteFournisseur(int id)
        {
            Fournisseur fournisseur = db.Fournisseur.Find(id);
            if (fournisseur == null)
            {
                return NotFound();
            }

            db.Fournisseur.Remove(fournisseur);
            db.SaveChanges();

            return Ok(fournisseur);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FournisseurExists(int id)
        {
            return db.Fournisseur.Count(e => e.IdFo == id) > 0;
        }
    }
}
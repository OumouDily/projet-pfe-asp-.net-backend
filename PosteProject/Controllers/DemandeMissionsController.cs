﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class DemandeMissionsController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/DemandeMissions
        public IQueryable<DemandeMission> GetDemandeMission()
        {
            return db.DemandeMission;
        }

     

        // PUT: api/DemandeMissions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDemandeMission(int id, DemandeMission demandeMission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != demandeMission.IdDM)
            {
                return BadRequest();
            }

            db.Entry(demandeMission).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DemandeMissionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DemandeMissions
        [ResponseType(typeof(DemandeMission))]
        public IHttpActionResult PostDemandeMission(DemandeMission demandeMission)
        {
           

            db.DemandeMission.Add(demandeMission);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = demandeMission.IdDM }, demandeMission);
        }

        // DELETE: api/DemandeMissions/5
        [ResponseType(typeof(DemandeMission))]
        public IHttpActionResult DeleteDemandeMission(int id)
        {
            DemandeMission demandeMission = db.DemandeMission.Find(id);
            if (demandeMission == null)
            {
                return NotFound();
            }

            db.DemandeMission.Remove(demandeMission);
            db.SaveChanges();

            return Ok(demandeMission);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DemandeMissionExists(int id)
        {
            return db.DemandeMission.Count(e => e.IdDM == id) > 0;
        }
    }
}
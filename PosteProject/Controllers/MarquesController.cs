﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PosteProject.Models;

namespace PosteProject.Controllers
{
    public class MarquesController : ApiController
    {
        private GestionParcDBEntities5 db = new GestionParcDBEntities5(true);

        // GET: api/Marques
        public IQueryable<Marque> GetMarque()
        {
            return db.Marque;
        }

     
        // PUT: api/Marques/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMarque(int id, Marque marque)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != marque.IdMa)
            {
                return BadRequest();
            }

            db.Entry(marque).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MarqueExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Marques
        [ResponseType(typeof(Marque))]
        public IHttpActionResult PostMarque(Marque marque)
        {
          
            db.Marque.Add(marque);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = marque.IdMa }, marque);
        }

        // DELETE: api/Marques/5
        [ResponseType(typeof(Marque))]
        public IHttpActionResult DeleteMarque(int id)
        {
            Marque marque = db.Marque.Find(id);
            if (marque == null)
            {
                return NotFound();
            }

            db.Marque.Remove(marque);
            db.SaveChanges();

            return Ok(marque);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MarqueExists(int id)
        {
            return db.Marque.Count(e => e.IdMa == id) > 0;
        }
    }
}